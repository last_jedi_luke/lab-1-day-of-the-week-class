#include <iostream>
#include <string>
//using namespace std;

class day_type {
private:
    int day_num;
    std::string day_string;
public:
    void set_day();
    void print_day (int number);
    void prev_day();
    void next_day();
    void add_days ();

};


int main() {


    day_type object;
    object.set_day();

    object.next_day();
    object.prev_day();
    object.add_days();




        return 0;
    }

void day_type::set_day() {
std::cout << "Set the day of the week: "
        "1 = Sunday"
        " 2 = Monday"
        " 3 = Tuesday"
        " 4 = Wednesday"
        " 5 = Thursday"
        " 6 = Friday"
        " 7 = Saturday" << std::endl;
int day = 0;
    for (int i = 0; i < 10; i++){
        std:: cin >> day;
        if (day <= 0 || day >= 8){
            std::cout << "Out of scope, try again..." << std::endl;
            continue;
        }

        else {
            break;
        }
    }



    day_num = day;

    std::cout << "Current day: " << std::endl;

    print_day (day_num);
}

void day_type::print_day(int number) {
    if (number == 1){
        day_string = "Sunday";
    }
    if (number == 2){
        day_string = "Monday";
    }
    if (number == 3){
        day_string = "Tuesday";
    }
    if (number == 4){
        day_string = "Wednesday";
    }
    if (number == 5){
        day_string = "Thursday";
    }
    if (number == 6){
        day_string = "Friday";
    }
    if (number == 7){
        day_string = "Saturday";
    }
   std:: cout << day_string << std::endl;
}

void day_type::prev_day(){



    std::cout << "Previous day: " << std::endl;
    int day_before = 0;
    int temp = 0;
    temp = day_num;
    day_before = (temp -1);
    //std::cout << day_before << std::endl;
    if (day_num == 1) {
        day_before = 7;

    }
    print_day(day_before);
}

void day_type::next_day(){
    std::cout << "Next day: " << std::endl;

    int temp = 0;
    int day_after = 0;
    temp = day_num;
    day_after = (temp +1);
    //std::cout << day_after <<std:: endl;
    if (day_num == 7) {
        day_after = 1;

    }
    print_day (day_after);

}

void day_type::add_days (){
    int days_added = 0;
    std::cout << "How many days would you like to add to the current day?" << std::endl;
    std::cin >> days_added;
    int temp = 0;
    temp = (days_added + day_num) % 7;
    print_day(temp);
}



//Code output:

/*Set the day of the week: 1 = Sunday 2 = Monday 3 = Tuesday 4 = Wednesday 5 = Thursday 6 = Friday 7 = Saturday
0
0
Out of scope, try again...
8
8
Out of scope, try again...
1
1
Current day:
Sunday
        Next day:
        Monday
Previous day:
Saturday
        How many days would you like to add to the current day?
12
12
Friday*/
